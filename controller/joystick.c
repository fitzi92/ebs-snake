#include "joystick.h"


struct joystick_mappings {
	uint32_t i2c_reset_base;
	uint32_t i2c_reset_peripheral;
	uint32_t i2c_reset_pin;

	uint32_t i2c_modul;
};

struct joystick_mappings joystick_config[JOYSTICK_BOOSTER_COUNT] = {
		//booster 1
		{
			.i2c_reset_base = GPIO_PORTC_BASE,
			.i2c_reset_peripheral = SYSCTL_PERIPH_GPIOC,
			.i2c_reset_pin = GPIO_PIN_7,
			.i2c_modul = I2CM_7
		},
		//booster 2
		{
			.i2c_reset_base = GPIO_PORTP_BASE,
			.i2c_reset_peripheral = SYSCTL_PERIPH_GPIOP,
			.i2c_reset_pin = GPIO_PIN_4,
			.i2c_modul = I2CM_8
		}
};

struct joystick_mappings *joystick = 0;

void init_joystick(uint8_t boosterpack){

	/*
	 * After Power Up, the following sequence must be performed:
	 * 1. VDD and VDDp Power up, and reached their nominal values (VDD>2.7V, VDDp>1.7V).
	 * 2. RESETn LOW during >100ns
	 * 3. Delay 1000μs
	 * 4. Loop check register [0Fh] until the value F0h or F1h is present (reset finished, registers to their default values)
	 * 5. Optional: Write value 86h into register [2Eh] → Invert magnet polarity. See Control Register 2 (2Eh) on page 22.
	 * 6. Configure register [2Bh] → Configure M_ctrl middle hall element control
	 * 7. Configure register [2Ch] → Configure J_ctrl attenuation factor
	 * 8. Configure register [2Dh] → Configure T_ctrl scaling factor
	 * 9. Configure the wanted Power Mode into register [0Fh] (Idle mode or Low Power Mode with Timebase configuration)
	 * 10. X Y coordinates are ready to be read.
	 */

	if(boosterpack == 0 || boosterpack == 1){
		joystick = &joystick_config[boosterpack];
	}

	//2.
	SysCtlPeripheralEnable(joystick->i2c_reset_peripheral);
	GPIOPinTypeGPIOOutput(joystick->i2c_reset_base,joystick->i2c_reset_pin);
	GPIOPinWrite(joystick->i2c_reset_base, joystick->i2c_reset_pin, 0);

	//2.2.
	GPIOPinWrite(joystick->i2c_reset_base, joystick->i2c_reset_pin, 1 << 7);

	//4.
	buf[0] = C1_REG;
	buf[1] = 0xF0;
	i2c_write(joystick->i2c_modul, buf, 2, I2C_SLAVE_ADDR, BLOCKING);

	while(1){
		buf[0] = C1_REG;
		buf[1] = 0;
		i2c_writeread(joystick->i2c_modul, buf, 1, 1, I2C_SLAVE_ADDR, BLOCKING);

		if(buf[1] == 0xF0 || buf[1] == 0xF1){
			break;
		}
	}

	//6.
	buf[0] = _M_CTRL;
	buf[1] = 0x00;
	i2c_write(joystick->i2c_modul, buf, 2, I2C_SLAVE_ADDR, BLOCKING);

	//7.
	buf[0] = _J_CTRL;
	buf[1] = 0x06;
	i2c_write(joystick->i2c_modul, buf, 2, I2C_SLAVE_ADDR, BLOCKING);

	//8.
	buf[0] = _T_CTRL;
	buf[1] = 0x09;
	i2c_write(joystick->i2c_modul, buf, 2, I2C_SLAVE_ADDR, BLOCKING);

	//9.
	buf[0] = C1_REG;
	buf[1] = 0b01110100;
	i2c_write(joystick->i2c_modul, buf, 2, I2C_SLAVE_ADDR, BLOCKING);

	//Configuring the DeadZone of the Joystick
	buf[0] = _XP;
	buf[1] = 16;
	i2c_write(joystick->i2c_modul, buf, 2, I2C_SLAVE_ADDR, BLOCKING);
	buf[0] = _XN;
	buf[1] = -16;
	i2c_write(joystick->i2c_modul, buf, 2, I2C_SLAVE_ADDR, BLOCKING);
	buf[0] = _YP;
	buf[1] = 16;
	i2c_write(joystick->i2c_modul, buf, 2, I2C_SLAVE_ADDR, BLOCKING);
	buf[0] = _YN;
	buf[1] = -16;
	i2c_write(joystick->i2c_modul, buf, 2, I2C_SLAVE_ADDR, BLOCKING);

}

//joystick needs an initial read so that movement detection is available
void initial_read(){
	buf[0] = Y_REG;
	buf[1] = 0;
	i2c_write(joystick->i2c_modul, buf, 1,I2C_SLAVE_ADDR, BLOCKING);
	i2c_read(joystick->i2c_modul, buf, 1,I2C_SLAVE_ADDR, BLOCKING);
}

//reads X Coordinate from joystick connected to BoosterPack1
void getX(){
	buf[0] = X_REG;
	buf[1] = 0;
	i2c_write(joystick->i2c_modul, buf, 1,I2C_SLAVE_ADDR, BLOCKING);
	i2c_read(joystick->i2c_modul, buf, 1,I2C_SLAVE_ADDR, BLOCKING);

	x_reg = buf[0];
	if(x_reg >= 0){
		x_abs = x_reg;
		xnegative = false;
	} else {
		x_abs = -x_reg;
		xnegative = true;
	}
}

//reads Y Coordinate from joystick connected to BoosterPack1
void getY(){
	buf[0] = Y_REG;
	buf[1] = 0;
	i2c_write(joystick->i2c_modul, buf, 1,I2C_SLAVE_ADDR, BLOCKING);
	i2c_read(joystick->i2c_modul, buf, 1,I2C_SLAVE_ADDR, BLOCKING);

	y_reg = buf[0];

	if(y_reg >= 0){
		y_abs = y_reg;
		ynegative = false;
	} else {
		y_abs = -y_reg;
		ynegative = true;
	}
}

//calculates direction based on Coordinates: -1:error, 0:up, 1:right, 2:down, 3:left
void getDirection(){
	if(x_abs > y_abs){
		if(xnegative){
			direction = 1;
		} else{
			direction = 3;
		}
	} else if(x_abs < y_abs) {
		if(ynegative){
			direction = 0;
		} else{
			direction = 2;
		}
	} else {
		direction = -1;
	}
}



