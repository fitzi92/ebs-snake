/*
 * ledm.h
 *
 *  Created on: 12.04.2016
 *      Author: fitzi
 */

#ifndef LEDM_H_
#define LEDM_H_

#include <stdint.h>
#include <stdbool.h>
#include "inc/tm4c1294ncpdt.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/systick.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "driverlib/timer.h"
#include "driverlib/ssi.h"

#include "ledm.h"

uint32_t ledm_SSIBase = 0;
uint32_t ledm_CSPort = 0;
uint8_t ledm_CSPin = 0;


void ledm_init(uint32_t sysclock){

	SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI3);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOQ);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);

	// P5 SS
	GPIOPinTypeGPIOOutput(GPIO_PORTP_BASE,GPIO_PIN_5);
	GPIOPinWrite(GPIO_PORTP_BASE,GPIO_PIN_5,0b11111111);

	// P4 - LED test
	GPIOPinTypeGPIOOutput(GPIO_PORTP_BASE,GPIO_PIN_4);
	GPIOPinWrite(GPIO_PORTP_BASE,GPIO_PIN_4,0b11111111);

	// Set the pin muxing to SSI3 on port Q
	GPIOPinConfigure(GPIO_PQ0_SSI3CLK);
	GPIOPinConfigure(GPIO_PQ3_SSI3XDAT1);
	GPIOPinConfigure(GPIO_PQ2_SSI3XDAT0);
	GPIOPinTypeSSI(GPIO_PORTQ_BASE,GPIO_PIN_3|GPIO_PIN_2|GPIO_PIN_0);

	// Configure the SSI
	SSIClockSourceSet(SSI3_BASE, SSI_CLOCK_SYSTEM);
	SSIConfigSetExpClk(SSI3_BASE, sysclock, SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER, 250000, 16);

	// Enable the SSI module.
	SSIEnable(SSI3_BASE);

	// Channel select
	ledm_CSPort = GPIO_PORTP_BASE;
	ledm_CSPin = GPIO_PIN_5;
	ledm_select(false);

	// SSI base
	ledm_SSIBase = SSI3_BASE;
}

void ledm_select( bool state ){
	GPIOPinWrite(ledm_CSPort,ledm_CSPin, state ? 0b0 : 0b11111111 );
	SysCtlDelay(ledm_min_wait);
}

void ledm_update(){
	ledm_select(true);
	ledm_wait();
	ledm_select(false);
}

void ledm_set( uint8_t addr, uint8_t data ){
	ledm_select(true);
	SSIDataPut(SSI3_BASE, (0 | (addr << 8) | data) );
	ledm_select(false);
}

void ledm_wait(){
	SysCtlDelay(ledm_min_wait);
}

void ledm_set_intensity( uint8_t intensity ){
	ledm_set(ledm_addr_intensity, intensity);
}

void ledm_scan_limit( uint8_t intensity ){
	ledm_set(ledm_addr_scanlimit, intensity);
}

void ledm_test_mode( bool state ){
	ledm_set(ledm_addr_test, (state ? 1 : 0));
}

void ledm_shutdown_mode( bool state ){
	ledm_set(ledm_addr_shutdown, (state ? 0 : 1));
}

void ledm_clear_all(){
	int i = 0;
	uint8_t addr = 1;
	for( ; i < 8; i++ ){
		ledm_set( addr, 0 );
		addr++;
		SysCtlDelay(ledm_min_wait);
	}
	ledm_update();
}

#endif /* LEDM_H_ */
