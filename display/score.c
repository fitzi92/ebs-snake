/*
 * score.c
 * Implementation of score.h. Uses hardcoded pins on boosterpack 1.
 */
#include <stdint.h>
#include <stdbool.h>

#include "score.h"

#include "inc/tm4c1294ncpdt.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"

void score_init() {
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);

	GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
	GPIOPinTypeGPIOOutput(GPIO_PORTG_BASE, GPIO_PIN_0);
	GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_4 | GPIO_PIN_5);

	uint32_t ui32Strength;
	uint32_t ui32PinType;

	GPIOPadConfigGet(GPIO_PORTF_BASE, GPIO_PIN_1, &ui32Strength, &ui32PinType);
	GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3,
			ui32Strength, GPIO_PIN_TYPE_STD);
	GPIOPadConfigSet(GPIO_PORTG_BASE, GPIO_PIN_0, ui32Strength,
			GPIO_PIN_TYPE_STD);
	GPIOPadConfigSet(GPIO_PORTL_BASE, GPIO_PIN_4 | GPIO_PIN_5, ui32Strength,
			GPIO_PIN_TYPE_STD);
}

void score_write(uint8_t score) {
	while (score > MAX_SCORE);

	/*
	 * Shift the value out to each pin, shifting by the delta between
	 * the pin number and the bit position in the score value.
	 */
	GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, score << 1);
	GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, score << 1);
	GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, score << 1);
	GPIOPinWrite(GPIO_PORTG_BASE, GPIO_PIN_0, score >> 3);
	GPIOPinWrite(GPIO_PORTL_BASE, GPIO_PIN_4, score);
	GPIOPinWrite(GPIO_PORTL_BASE, GPIO_PIN_5, score);
}
