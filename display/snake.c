#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "inc/tm4c1294ncpdt.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/systick.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "driverlib/timer.h"
#include "driverlib/ssi.h"

#include "snake.h"

#include "ledm.h"
#include "score.h"


// holds currents game status:
// 0: Game active or stopped
// 1: Input waiting
// 2: Input: Click
// 3 + 4: Input: (00 up, 01 right, 10 bottom, 11 left)
// -
// 5: dead
// 6: unused
// 7 + 8: Current direction (00 north, 01 east, 10 south, 11 west)
// -
// 9 + 10 + 11 : 0->7 x coordinate of food
// -
// 12 + 13 + 14: 0->7 y coordinate of food
// -
// 15: initialized

volatile uint16_t snake_state = 0;			// currents game status
volatile uint8_t snake_points = 0;			// points in current game (or current animation frame, if in dead-mode)
volatile struct snake *head;				// pointer to the head of the snake
volatile struct snake *tail;				// pointer to the tail
volatile uint8_t board[8] = {0};			// representation of the board

volatile uint8_t snake_random_x;
volatile uint8_t snake_random_a;
volatile uint8_t snake_random_b;
volatile uint8_t snake_random_c;

/* Initialize snake
 * must not be called more than once
 * allocates memory for snake nodes
 * and makes a connected list
 * */
void snake_init(){

	if( snake_state_get(snake_state_initialized) )
		return;

	struct snake *first_node;
	struct snake *last_node;
	struct snake *prev_node;

	uint8_t i = 0;

	// [ 1 ]-prev->[ 2 ]-prev->[ 3 ] ... [ 62 ]-prev->[ 63 ]-prev->[ 64 ](-prev->[1])

	// create 64 snake nodes
	for( ; i < 64; i++ ){

		struct snake *node = (struct snake*)malloc(sizeof(struct snake)); // Activate Project Properties > Build > Linker > Basic Options > Heap Size (1200)

		node->position = 0;

		if( i == 0 ){
			first_node = node;
		}else if( i == 63 ){
			last_node = node;
		}

		if( i > 0 ){
			node->next = prev_node;
			prev_node->prev = node;
		}

		prev_node = node;

	}

	// node should build a circle
	first_node->next = last_node;
	last_node->prev = first_node;

	head = first_node;
	tail = head->prev;
	head->position = 0b10100011; // 4/3
	tail->position = 0b10100100; // 4/4

	snake_state_set(snake_state_initialized, true);

	// set timer
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
	TimerClockSourceSet(TIMER0_BASE, TIMER_CLOCK_SYSTEM);
	TimerIntRegister( TIMER0_BASE, TIMER_BOTH, snake_isr );
	TimerIntEnable( TIMER0_BASE, TIMER_TIMA_TIMEOUT );
	TimerLoadSet(TIMER0_BASE, TIMER_BOTH, snake_frames_menu);
	TimerEnable(TIMER0_BASE, TIMER_BOTH);

}

void snake_set_speed( uint32_t speed ){
	// Debounce buttons with timer
	TimerDisable(TIMER0_BASE, TIMER_BOTH);
	TimerLoadSet(TIMER0_BASE, TIMER_BOTH, speed);
	TimerEnable(TIMER0_BASE, TIMER_BOTH);
}

/* Resets the game to its initial state
 * */
void snake_reset(){

	uint8_t i = 0;
	volatile struct snake *node = head;
	for( ; i < 64; i++){
		node->position = 0;
		node = node->next;
	}

	tail = head->prev;
	head->position = 0b10100011; // 4/3
	tail->position = 0b10100100; // 4/4

	snake_state = 0;
	snake_points = 0;
	score_write(0);

}

/* Starts the game
 * */
void snake_start(){

	// reset game
	snake_reset();

	// generate food
	snake_generate_food();

	// set status to active
	snake_state_set(snake_state_active, true);

	// update speed
	snake_set_speed(snake_frames_xs);

	// render screen
	snake_render_game();

}

/* Stops the game
 * */
void snake_stop(){

	snake_state = 0;
	snake_points = 0;

	snake_set_speed(snake_frames_menu);

}

extern void snake_isr(void){
	TimerIntClear(TIMER0_BASE, TIMER_BOTH);

	if( snake_state_get(snake_state_active) ){

		if( snake_state_get(snake_state_dead) ){
			snake_game_dead();
		}else{
			snake_update_game();
		}

	}else{

		snake_update_welcome();

		// pseudo randomize pseudo random number generator
		snake_points++;
		snake_init_randomizer(snake_points, snake_state_get_input_direction(), ((snake_points << 1) & snake_state_get_input_direction()));

	}

}

void snake_update_welcome(){

	uint8_t welcome[8] = {
		0b01111110,
		0b11000011,
		0b11000000,
		0b01111110,
		0b00000011,
		0b11000011,
		0b11000011,
		0b01111110
	};

	//ledm_set_intensity(ledm_intensity_min);
	//ledm_wait();
	ledm_set(ledm_addr_digit0, welcome[0]);
	ledm_wait();
	ledm_set(ledm_addr_digit1, welcome[1]);
	ledm_wait();
	ledm_set(ledm_addr_digit2, welcome[2]);
	ledm_wait();
	ledm_set(ledm_addr_digit3, welcome[3]);
	ledm_wait();
	ledm_set(ledm_addr_digit4, welcome[4]);
	ledm_wait();
	ledm_set(ledm_addr_digit5, welcome[5]);
	ledm_wait();
	ledm_set(ledm_addr_digit6, welcome[6]);
	ledm_wait();
	ledm_set(ledm_addr_digit7, welcome[7]);
	ledm_wait();
	ledm_update();

}

void snake_update_game(){

	if( !snake_state_get(snake_state_active) )
		return;

	uint8_t curr_x = snake_get_curr_head_position(snake_pos_x);
	uint8_t curr_y = snake_get_curr_head_position(snake_pos_y);

	// check input
	if( snake_state_get(snake_state_input_waiting) ){

		// read new direction and save it
		uint8_t curdir = snake_state_get_current_direction();
		uint8_t newdir = snake_state_get_input_direction();

		uint8_t allowed1 = curdir == 0 ? 3 : curdir-1;
		uint8_t allowed2 = curdir == 3 ? 0 : curdir+1;

		if( newdir == allowed1 || newdir == allowed2 ){
			snake_state_set_current_direction( newdir );
		}// else ignore

		// reset waiting bit
		snake_state_set(snake_state_input_waiting, false);

	}

	// get direction
	uint8_t direction = snake_state_get_current_direction();


	// GO UP/TOP
	if( direction == snake_dir_top ){


		if( curr_y == 0 || !snake_is_empty_node(curr_x, curr_y - 1) ){
			snake_game_dead();
			return;
		}

		curr_y--;

		snake_add_next_position(curr_x, curr_y);

	}else

	// GO RIGHT
	if( direction == snake_dir_right ){


		if( curr_x == snake_field_w - 1 || !snake_is_empty_node(curr_x + 1, curr_y) ){
			snake_game_dead();
			return;
		}

		curr_x++;

		snake_add_next_position(curr_x, curr_y);

	}else

	// GO DOWN/BOTTOM
	if( direction == snake_dir_bottom ){

		if( curr_y == snake_field_h - 1 || !snake_is_empty_node(curr_x, curr_y + 1) ){
			snake_game_dead();
			return;
		}

		curr_y++;

		snake_add_next_position(curr_x, curr_y);

	}else

	// GO LEFT
	if( direction == snake_dir_left ){


		if( curr_x == 0 || !snake_is_empty_node(curr_x - 1, curr_y) ){
			snake_game_dead();
			return;
		}

		curr_x--;

		snake_add_next_position(curr_x, curr_y);

	}else{
		// something went wrong
	}

	uint8_t food_x = (snake_state >> snake_food_pos_x) & 0b111;
	uint8_t food_y = (snake_state >> snake_food_pos_y) & 0b111;

	// check if snake ate food
	if( food_x == curr_x && food_y == curr_y ){
		snake_generate_food();
		snake_points++;
		score_write( snake_points );
	}else{
		snake_shorten_tail();
	}

	// update speed on specific points
	switch( snake_points ){
		case 4:
			snake_set_speed(snake_frames_s);
			break;
		case 8:
			snake_set_speed(snake_frames_m);
			break;
		case 16:
			snake_set_speed(snake_frames_l);
			break;
		case 32:
			snake_set_speed(snake_frames_xl);
			break;
	}

	// render game
	snake_render_game();

}

uint8_t snake_get_curr_head_position( uint8_t axis ){
	return (head->position >> axis) & 0b111;
}

void snake_add_next_position( uint8_t x, uint8_t y ){
	head = head->next;
	head->position = (1 << 7) | ((x & 0b111) << 3) | (y & 0b111);
}

void snake_shorten_tail(){
	tail->position = 0;
	tail = tail->next;
}

bool snake_state_get( uint8_t position ){
	return ((snake_state & ( 1 << position )) >> position) == 1 ? true : false;
}

uint8_t snake_state_get_current_direction(){
	return (uint8_t)( (snake_state >> snake_state_direction) & 0b11 );
	//return (uint8_t)((snake_state << 7) >> 14);
}

uint8_t snake_state_get_input_direction(){
	return (uint8_t)( (snake_state >> snake_state_input_direction) & 0b11 );
	//return (uint8_t)((snake_state << 7) >> 14);
}

void snake_state_set_current_direction( uint8_t dir ){
	// clear current direction
	snake_state = snake_state & ~( 0b11 << snake_state_direction );
	// set new direction
	snake_state = snake_state | ( (0b11 & dir) << snake_state_direction );
}

void snake_state_set( uint8_t position, bool value ){
	if( value )
		snake_state = snake_state | (1 << position);
	else
		snake_state = snake_state & ~(1 << position);
}


void snake_render_game(){

	// clear board
	int i = 0;
	for( ; i < 8; i++ )
		board[i] = 0;

	// cycle snake
	volatile struct snake *node = head;
	while(1){

		uint8_t x = (node->position >> 3) & 0b111;
		uint8_t y = node->position & 0b111;

		board[y] = board[y] | 0b1 << x;

		if( node == tail )
			break;

		node = node->prev;
	}

	// add food
	uint8_t food_x = (snake_state >> snake_food_pos_x) & 0b111;
	uint8_t food_y = (snake_state >> snake_food_pos_y) & 0b111;
	board[food_y] = board[food_y] | 0b1 << food_x;

	snake_render_board();
}

void snake_render_board(){
	int i = 0;
	for( ; i < 8; i++ ){
		ledm_set(i+1, board[i]);
		ledm_wait();
	}
	ledm_update();
}

void snake_game_dead(){

	// if called first time
	if( !snake_state_get(snake_state_dead) ){
		snake_points = 0;
		snake_state_set(snake_state_dead, true);
		snake_set_speed(snake_frames_animation);
	}

	if( snake_points < 8 ){

		board[0] = 0b01100110;
		board[1] = 0b11111111;
		board[2] = 0b11111111;
		board[3] = 0b11111111;
		board[4] = 0b11111111;
		board[5] = 0b01111110;
		board[6] = 0b00111100;
		board[7] = 0b00011000;

		uint8_t heart_empty[8] = {
			0b01100110,
			0b10011001,
			0b10000001,
			0b10000001,
			0b10000001,
			0b01000010,
			0b00100100,
			0b00011000
		};

		uint8_t i = 1;
		for( ; i < snake_points + 1; i++ ){
			board[i] = heart_empty[i];
		}

	}else{

		switch( snake_points ){
			case 8:
				board[0] = 0b01100110;
				board[1] = 0b10011011;
				board[2] = 0b11000000;
				board[3] = 0b01000000;
				board[4] = 0b00000001;
				board[5] = 0b01000011;
				board[6] = 0b00000000;
				board[7] = 0b00111100;
				break;

			case 9:
				board[0] = 0b01111110;
				board[1] = 0b11000011;
				board[2] = 0b11000000;
				board[3] = 0b01100010;
				board[4] = 0b00000011;
				board[5] = 0b11000011;
				board[6] = 0b01000010;
				board[7] = 0b00111100;
				break;

			case 10:
				board[0] = 0b01111110;
				board[1] = 0b11000011;
				board[2] = 0b11000000;
				board[3] = 0b01110110;
				board[4] = 0b00000011;
				board[5] = 0b11000011;
				board[6] = 0b11000011;
				board[7] = 0b01111110;
				break;

			case 11:
				board[0] = 0b01111110;
				board[1] = 0b11000011;
				board[2] = 0b11000000;
				board[3] = 0b01111110;
				board[4] = 0b00000011;
				board[5] = 0b11000011;
				board[6] = 0b11000011;
				board[7] = 0b01111110;
				snake_stop();
				break;
		}

	}

	snake_render_board();

	snake_points++;

}

void snake_generate_food(){

	uint8_t x = 0;
	uint8_t y = 0;

	do{
		x = snake_get_random() & 0b111;
		y = snake_get_random() & 0b111;

	}while( !snake_is_empty_node(x, y) );

	snake_state = snake_state & ~(0b111111 << snake_food_pos_x);
	snake_state = snake_state | (x << snake_food_pos_x) | (y << snake_food_pos_y);

}

bool snake_is_empty_node( uint8_t x, uint8_t y ){

	volatile struct snake *node = head;
	while(1){

		uint8_t node_x = (node->position >> 3) & 0b111;
		uint8_t node_y = node->position & 0b111;

		if( node_x == x && node_y == y )
			return false;

		if( node == tail )
			break;

		node = node->prev;
	}

	return true;
}

void snake_input_click(){
	if( !snake_state_get(snake_state_active) ){
		snake_start();
	}else{
		snake_state_set(snake_state_input_click, true);
		snake_state_set(snake_state_input_waiting, true);
	}
}

void snake_input( uint8_t direction ){
	// clear current direction
	snake_state = snake_state & ~(0b11 << 3);
	// set new direction
	snake_state = snake_state | ((direction & 0b11) << 3);
	// set waiting input flag
	snake_state_set(snake_state_input_waiting, true);
}


void snake_init_randomizer(uint8_t s1, uint8_t s2, uint8_t s3) {
	snake_random_a ^= s1;
	snake_random_b ^= s2;
	snake_random_c ^= s3;
	snake_random_x++;
	snake_random_a = (snake_random_a^snake_random_c^snake_random_x);
	snake_random_b = (snake_random_b+snake_random_a);
	snake_random_c = (snake_random_c+(snake_random_b>>1)^snake_random_a);
}

uint8_t snake_get_random(){
	snake_random_x++;               										//x is incremented every round and is not affected by any other variable
	snake_random_a = (snake_random_a^snake_random_c^snake_random_x);       	//note the mix of addition and XOR
	snake_random_b = (snake_random_b+snake_random_a);         				//And the use of very few instructions
	snake_random_c = (snake_random_c+(snake_random_b>>1)^snake_random_a);  	//the right shift is to ensure that high-order bits from b can affect
	return snake_random_c;          										//low order bits of other variables
}
