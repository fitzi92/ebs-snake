/*
 * i2c_wrapper.h
 *
 *  Created on: Apr 20, 2015
 *      Author: matthias

 *	Bugfix considering transfer struct: May 21, 2015, matthias - thx to michael for noticing
 *	Modifications: michael, 09.06.2015
 *
 *      i2c wrapper driver for the TM4C1294NCPDT mounted on the EK-TM4C1294XL
 *      uses tiva ware i2c api
 *      currently supported:
 *      blocking/nonblocking read/write with software buffers
 *      for I2C0, I2C2, I2C7, I2C8
 */

#ifndef I2C_WRAPPER_H_
#define I2C_WRAPPER_H_


#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "inc/tm4c1294ncpdt.h"
#include "driverlib/gpio.h"
#define PART_TM4C1294NCPDT
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include <driverlib/i2c.h>

/*device_ids*/
#define I2CM_0 0
#define I2CM_2 1
#define I2CM_7 2
#define I2CM_8 3

#define LOW_100_kHz false
#define HIGH_400_kHz true

#define BLOCKING 1
#define NON_BLOCKING 0

#define MAX_DEVICES 4

struct transfer {
    uint32_t base_addr;
    uint8_t slave_addr;
    uint8_t *buf;
    uint8_t wlen;
    uint8_t rlen;
    volatile uint8_t ptr;
    volatile enum {WRITE,READ,IDLE} state;
};



struct i2c_mappings {
    uint32_t base_addr;
    uint32_t en_i2c;
    uint32_t en_gpio;
    uint32_t scl_pincfg;
    uint32_t sda_pincfg;
    uint32_t scl_sda_gpio_port;
    uint32_t scl_gpio_pin;
    uint32_t sda_gpio_pin;
};

typedef void (* irqfnTable_t)(void);

/*common interrupt function*/
void i2c_device_handler(struct transfer *t);

/*master specific interrupt routines*/
void i2c0_isr(void);
void i2c2_isr(void);
void i2c7_isr(void);
void i2c8_isr(void);

uint32_t i2c_init(uint8_t device_id,uint32_t sysclock_freq, bool bfast);
uint32_t i2c_write(uint8_t device_id, uint8_t *buf, uint8_t len,uint8_t slave_addr,uint8_t blocking);
uint32_t i2c_writeread(uint8_t device_id, uint8_t *buf, uint8_t wlen, uint8_t rlen, uint8_t slave_addr,uint8_t blocking);
uint32_t i2c_read(uint8_t device_id, uint8_t *buf, uint8_t len,uint8_t slave_addr,uint8_t blocking);


#endif /* I2C_WRAPPER_H_ */
