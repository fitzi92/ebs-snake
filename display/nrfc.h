/*
 * # nrfc.h
 *
 * This is the API/driver for nRF C click wireless modules used by Snake.
 *
 * It favors ease of use over flexibility:
 * Most of the initialization happens behind the scenes, users only have two choose a
 * booster pack and the function (receiver, transmitter) to use. The drawback is that
 * it probably is very specific to Tiva C TM4C1294 microcontrollers.
 *
 *
 * ## Assumptions / Limitations
 *  - The transmitter API is hardcoded to use Timer moduble 0.
 *  - The API has only been tested with clock speeds of 10MHz and 40Mhz.
 *
 * ## Example code
 * ### Receiver
 * nrfc_init_receiver(sysclock, NRFC_BOOSTER_1, switch_on_led);
 * IntMasterEnable();
 *
 * ### Transmitter
 * nrfc_init_transmitter(sysclock, NRFC_BOOSTER_2);
 * nrfc_send_byte(byte);
 */

#ifndef NRFC_API_NRFC_H_
#define NRFC_API_NRFC_H_

#include <stdint.h>

#define NRFC_BOOSTER_1 0
#define NRFC_BOOSTER_2 1

/**
 * Initializes an nRF C click to act as a transmitter.
 *
 * Parameters:
 * 	 sysclock: The clock speed.
 * 	 booster_slot: The booster slot the click is connected to. One of NRFC_BOOSTER_*.
 *
 * For assumptions and limitations in this API, please refer to the top-level documentation in the header file.
 */
void nrfc_init_transmitter(uint32_t sysclock, uint8_t booster_slot);

/**
 * Initializes an nRF C click to act as a receiver.
 *
 * Parameters:
 * 	 sysclock: The clock speed.
 * 	 booster_slot: The booster slot the click is connected to. One of NRFC_BOOSTER_*.
 * 	 received_byte_handler: Pointer to a function that is called for each byte received.
 * 	 						NOTE: This function will be called in an interrupt service routine!
 *
 * For assumptions and limitations in this API, please refer to the top-level documentation in the header file.
 */
void nrfc_init_receiver(uint32_t sysclock, uint8_t booster_slot,
		void (*received_byte_handler)(uint8_t));

/**
 * Sends a single byte via the nRF C click initialized via nrfc_init_transmitter(...)
 *
 * Parameters:
 * 	 byte: The value to send.
 *
 */
void nrfc_send_byte(uint8_t byte);

#endif /* NRFC_API_NRFC_H_ */
