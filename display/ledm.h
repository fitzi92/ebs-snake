#include <stdint.h>
#include <stdbool.h>

// led_min_wait must represent at least 50ns
#ifndef ledm_min_wait
	#define ledm_min_wait 500
#endif

// register adresses
#define ledm_addr_noop 		0b0000
#define ledm_addr_digit0 	0b0001
#define ledm_addr_digit1	0b0010
#define ledm_addr_digit2 	0b0011
#define ledm_addr_digit3 	0b0100
#define ledm_addr_digit4 	0b0101
#define ledm_addr_digit5 	0b0110
#define ledm_addr_digit6 	0b0111
#define ledm_addr_digit7 	0b1000
#define ledm_addr_decode 	0b1001
#define ledm_addr_intensity 0b1010
#define ledm_addr_scanlimit 0b1011
#define ledm_addr_shutdown 	0b1100
#define ledm_addr_test 		0b1111

// decode mode register data
#define ledm_decode_nodecode	0b00000000

// scan limit
#define ledm_scanlimit_all	0b00000111

// brightness
#define ledm_intensity_min	0b00000000
#define ledm_intensity_max	0b00001111

void ledm_init(uint32_t sysclock);
void ledm_select( bool state );
void ledm_update(void);
void ledm_wait();
void ledm_set( uint8_t addr, uint8_t data );
void ledm_set_intensity( uint8_t intensity );
void ledm_scan_limit( uint8_t intensity );
void ledm_test_mode( bool state );
void ledm_shutdown_mode( bool state );
void ledm_clear_all();
