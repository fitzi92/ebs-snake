/*
 * i2c_wrapper.c
 *
 *  Created on: Apr 20, 2015
 *      Author: matthias, 
 * 		Modifications: michael, 09.06.2015, matthias, 29.3.2016
 */

#include "i2c_wrapper.h"


static struct transfer g_transfer_i2c[4];

irqfnTable_t isr_table[MAX_DEVICES] = {
        i2c0_isr,
        i2c2_isr,
        i2c7_isr,
        i2c8_isr,
};

struct i2c_mappings i2c_attributes[MAX_DEVICES] = {
     /*i2c0*/{I2C0_BASE, SYSCTL_PERIPH_I2C0,SYSCTL_PERIPH_GPIOB , GPIO_PB2_I2C0SCL, GPIO_PB3_I2C0SDA, GPIO_PORTB_BASE, GPIO_PIN_2, GPIO_PIN_3},
     /*i2c2*/{I2C2_BASE, SYSCTL_PERIPH_I2C2, SYSCTL_PERIPH_GPION, GPIO_PN5_I2C2SCL, GPIO_PN4_I2C2SDA, GPIO_PORTN_BASE, GPIO_PIN_5, GPIO_PIN_4},
     /*i2c7*/{I2C7_BASE, SYSCTL_PERIPH_I2C7, SYSCTL_PERIPH_GPIOD, GPIO_PD0_I2C7SCL, GPIO_PD1_I2C7SDA, GPIO_PORTD_BASE, GPIO_PIN_0, GPIO_PIN_1},
     /*i2c8*/{I2C8_BASE, SYSCTL_PERIPH_I2C8, SYSCTL_PERIPH_GPIOA, GPIO_PA2_I2C8SCL, GPIO_PA3_I2C8SDA, GPIO_PORTA_BASE, GPIO_PIN_2, GPIO_PIN_3}
};


void i2c0_isr(void) 
{
    i2c_device_handler(&g_transfer_i2c[I2CM_0]);
}

void i2c2_isr(void) 
{
    i2c_device_handler(&g_transfer_i2c[I2CM_2]);
}

void i2c7_isr(void) 
{
    i2c_device_handler(&g_transfer_i2c[I2CM_7]);
}

void i2c8_isr(void) 
{
    i2c_device_handler(&g_transfer_i2c[I2CM_8]);
}

uint32_t i2c_init(uint8_t device_id,uint32_t sysclock_freq, bool bfast) 
{

    struct i2c_mappings *dev = NULL;
    struct transfer *i2c_handle = NULL;
    void (*i2c_isr)(void) = NULL;
    if(device_id > (MAX_DEVICES-1))
        return 0;

    dev  = (struct i2c_mappings *)&i2c_attributes[device_id];
    i2c_handle = (struct transfer *)&g_transfer_i2c[device_id];
    i2c_isr = isr_table[device_id];

    /*init our handle*/
    i2c_handle->base_addr = dev->base_addr;
    i2c_handle->buf = NULL;
    i2c_handle->wlen = 0;
    i2c_handle->rlen = 0;
    i2c_handle->ptr = 0;
    i2c_handle->slave_addr = 0;
    i2c_handle->state = IDLE;

    SysCtlPeripheralEnable(dev->en_i2c);

    SysCtlPeripheralEnable(dev->en_gpio);
    GPIOPinConfigure(dev->scl_pincfg);
    GPIOPinConfigure(dev->sda_pincfg);
    GPIOPinTypeI2CSCL(dev->scl_sda_gpio_port,dev->scl_gpio_pin);
    GPIOPinTypeI2C(dev->scl_sda_gpio_port,dev->sda_gpio_pin);

    I2CMasterInitExpClk(dev->base_addr,sysclock_freq,bfast);

    I2CIntRegister (dev->base_addr,i2c_isr);
    I2CMasterIntEnableEx(dev->base_addr,I2C_MASTER_INT_DATA | I2C_MASTER_INT_STOP);

    I2CMasterEnable(dev->base_addr);

    return 1;
}



uint32_t i2c_write(uint8_t device_id, uint8_t *buf, uint8_t len,uint8_t slave_addr,uint8_t blocking) 
{

    struct transfer *t = (struct transfer *)&g_transfer_i2c[device_id];
    uint32_t command;

    /*i2c is busy*/
    if(blocking == NON_BLOCKING) {
        if(t->state != IDLE)
            return 0;
    }


    t->buf = buf;
    t->wlen = len;
    t->rlen = 0;
    t->slave_addr = slave_addr;//provide as slave_addr >> 1 from computed slave address

    if(t->wlen > 1) {
        command = I2C_MASTER_CMD_BURST_SEND_START;
    } else if(t->wlen > 0) {
        command = I2C_MASTER_CMD_SINGLE_SEND;
    } else {
        return 0;
    }

    t->state = WRITE;
    t->ptr = 0;

    I2CMasterSlaveAddrSet(t->base_addr,t->slave_addr,false);
    I2CMasterDataPut(t->base_addr,t->buf[t->ptr++]);
    I2CMasterControl(t->base_addr,command);

    /*block until finished*/
    if(blocking == BLOCKING) {
        while(t->state != IDLE);
    }

    return 1;

}

uint32_t i2c_writeread(uint8_t device_id, uint8_t *buf, uint8_t wlen, uint8_t rlen, uint8_t slave_addr,uint8_t blocking) 
{

    struct transfer *t = (struct transfer *)&g_transfer_i2c[device_id];
    uint32_t command;

    /*i2c is busy*/
    if(blocking == NON_BLOCKING) {
        if(t->state != IDLE)
            return 0;
    }

    t->buf = buf;
    t->wlen = wlen;
    t->rlen = rlen;
    t->slave_addr = slave_addr;//provide as slave_addr >> 1 from computed slave address
    t->state = WRITE;
    t->ptr = 0;

    if((wlen+rlen) > 1) {
        command = I2C_MASTER_CMD_BURST_SEND_START;
    }  else {
        return 0;
    }
        I2CMasterSlaveAddrSet(t->base_addr,t->slave_addr,false);
        I2CMasterDataPut(t->base_addr,t->buf[t->ptr++]);
        I2CMasterControl(t->base_addr,command);

    /*block until write has finished*/
    while(t->state != READ);
   
	if(t->rlen > 1) {
		 command = I2C_MASTER_CMD_BURST_RECEIVE_CONT;
	 } else if(t->rlen > 0) {
		 command = I2C_MASTER_CMD_SINGLE_RECEIVE;
	 } else {
		 return 0;
	 }
		I2CMasterSlaveAddrSet(t->base_addr,t->slave_addr,true);
		I2CMasterControl(t->base_addr,command);

		/*block until finished*/
		if(blocking == BLOCKING) {
			while(t->state != IDLE);
		}

    return 1;

}



uint32_t i2c_read(uint8_t device_id, uint8_t *buf, uint8_t len,uint8_t slave_addr,uint8_t blocking)
{

    struct transfer *t = (struct transfer *)&g_transfer_i2c[device_id];
    uint32_t command;

    /*i2c is busy*/
    if(blocking == NON_BLOCKING) {
        if(t->state != IDLE)
            return 0;
    }

    t->buf = buf;
    t->wlen = 0;
    t->rlen = len;
    t->slave_addr = slave_addr;//provide as slave_addr >> 1 from computed slave address


    if(t->rlen > 1) {
        command = I2C_MASTER_CMD_BURST_RECEIVE_START;
    } else if(t->rlen > 0) {
        command = I2C_MASTER_CMD_SINGLE_RECEIVE;
    } else {
        return 0;
    }

    t->state = READ;
    t->ptr = 0;

    I2CMasterSlaveAddrSet(t->base_addr,t->slave_addr,true);
    I2CMasterControl(t->base_addr,command);

    /*block until finished*/
    if(blocking == BLOCKING) {
        while(t->state != IDLE);
    }

    return 1;

}

void i2c_device_handler(struct transfer *t) 
{
    uint32_t s,err;

    s = I2CMasterIntStatusEx(t->base_addr,false);
    I2CMasterIntClearEx(t->base_addr,s);
    I2CMasterIntClear(t->base_addr);
    err = I2CMasterErr(t->base_addr);
    if(err != I2C_MASTER_ERR_NONE) {
        if(t->state == WRITE)
            I2CMasterControl(t->base_addr,I2C_MASTER_CMD_BURST_SEND_ERROR_STOP);
        if(t->state == READ)
            I2CMasterControl(t->base_addr,I2C_MASTER_CMD_BURST_RECEIVE_ERROR_STOP);
    }

    /*uncomment for future use*/
    /*
    if(s & I2C_MASTER_INT_RX_FIFO_FULL)
    { s = s;}
    if(s & I2C_MASTER_INT_TX_FIFO_EMPTY)
    { s = s;}
    if(s & I2C_MASTER_INT_RX_FIFO_REQ)
    { s = s;}
    if(s & I2C_MASTER_INT_TX_FIFO_REQ)
    { s = s;}
    if(s & I2C_MASTER_INT_ARB_LOST)
    { s = s;}
    if(s & I2C_MASTER_INT_START)
    { s = s;}
    if(s & I2C_MASTER_INT_NACK)
    { s = s;}
    if(s & I2C_MASTER_INT_TX_DMA_DONE)
    { s = s;}
    if(s & I2C_MASTER_INT_RX_DMA_DONE)
    { s = s;}
    */


    if(s & I2C_MASTER_INT_DATA) {

        switch(t->state) {
        case READ:
            /*last  byte read*/
            if( ((t->ptr)+1-(t->wlen)) == (t->rlen)) {
                t->buf[t->ptr++] = (uint8_t)I2CMasterDataGet(t->base_addr);
                /*read prior to last byte; prepare last byte receiption*/
            } else if(((t->ptr)+2-(t->wlen)) == (t->rlen)) {
                t->buf[t->ptr++] = (uint8_t)I2CMasterDataGet(t->base_addr);
                I2CMasterControl(t->base_addr,I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
            /*more than one byte left*/
            } else if(((t->ptr)+2-(t->wlen)) < (t->rlen)) {
                t->buf[t->ptr++] = (uint8_t)I2CMasterDataGet(t->base_addr);
                I2CMasterControl(t->base_addr,I2C_MASTER_CMD_BURST_RECEIVE_CONT);
                //UARTprintf("%d\n", t->buf[t->ptr++]);
            }
            break;
        case WRITE:
            /*more than one byte left to send*/
            if(((t->ptr)+1) < (t->wlen)) {
                I2CMasterDataPut(t->base_addr,t->buf[t->ptr++]);
                I2CMasterControl(t->base_addr,I2C_MASTER_CMD_BURST_SEND_CONT);
            /*send last byte*/
            } else if((t->ptr) < (t->wlen)) {
                I2CMasterDataPut(t->base_addr,t->buf[t->ptr++]);
                if (t->rlen==0) {
                	  I2CMasterControl(t->base_addr,I2C_MASTER_CMD_BURST_SEND_FINISH);
                }
            /* if last byte was sent, and there is something to read. don't go idle, but switch to reading mode*/
            } else if (t->rlen>0) {
   	            t->state = READ;
   	            return;
              }
            break;
        };
    }

    if(s & I2C_MASTER_INT_STOP) {
        s = s;
        t->state = IDLE;
        t->ptr = 0;

    }

}
