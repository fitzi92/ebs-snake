/*
 * nrfc.c
 * Implementation of nrfc.h
 */
#include "nrfc.h"
#include "nRF_defines.h"

#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/ssi.h"
#include "driverlib/timer.h"
#include "driverlib/gpio.h"

/* ###################################
 * Model definition
 * ################################### */

/**
 * Structure for configuring the SSI module and additional pins used
 * for communicating with nRF C clicks.
 */
struct _nrfc_mappings {
	uint32_t ssi_base;
	uint32_t ssi_periph;
	uint32_t ssi_gpio_periph;
	uint32_t ssi_gpio_base;

	uint32_t ssi_gpio_afsel_mosi;
	uint8_t ssi_gpio_mosi_pin;

	uint32_t ssi_gpio_afsel_miso;
	uint8_t ssi_gpio_miso_pin;

	uint32_t ssi_gpio_afsel_clk;
	uint8_t ssi_gpio_clk_pin;

	uint32_t cs_gpio_periph;
	uint32_t cs_gpio_base;
	uint8_t cs_gpio_pin;
	uint8_t cs_shift;

	uint32_t ce_gpio_periph;
	uint32_t ce_gpio_base;
	uint8_t ce_gpio_pin;
	uint8_t ce_shift;

	uint32_t int_gpio_periph;
	uint32_t int_gpio_base;
	uint8_t int_gpio_pin;
	uint8_t int_gpio_int_pin;
};

/* ###################################
 * Constants
 * ################################### */

#define _NRFC_PAYLOAD_WIDTH 2
#define _NRFC_BOOSTER_COUNT 2

uint8_t _NRFC_PIPE_ADDRESS[ADR_LENGTH] = { 0x34, 0x43, 0x10, 0x10, 0x01 };

struct _nrfc_mappings _nrfc_configs[_NRFC_BOOSTER_COUNT] = {
//booster 1
		{ .ssi_base = SSI2_BASE, //
				.ssi_periph = SYSCTL_PERIPH_SSI2, //
				.ssi_gpio_periph = SYSCTL_PERIPH_GPIOD, //
				.ssi_gpio_base = GPIO_PORTD_BASE, //

				.ssi_gpio_afsel_mosi = GPIO_PD0_SSI2XDAT1, //
				.ssi_gpio_mosi_pin = GPIO_PIN_0, //

				.ssi_gpio_afsel_miso = GPIO_PD1_SSI2XDAT0, //
				.ssi_gpio_miso_pin = GPIO_PIN_1, //

				.ssi_gpio_afsel_clk = GPIO_PD3_SSI2CLK, //
				.ssi_gpio_clk_pin = GPIO_PIN_3, //

				.cs_gpio_periph = SYSCTL_PERIPH_GPIOH, //
				.cs_gpio_base = GPIO_PORTH_BASE, //
				.cs_gpio_pin = GPIO_PIN_2, //
				.cs_shift = 2, //

				.ce_gpio_periph = SYSCTL_PERIPH_GPIOC, //
				.ce_gpio_base = GPIO_PORTC_BASE, //
				.ce_gpio_pin = GPIO_PIN_7, //
				.ce_shift = 7,

				.int_gpio_periph = SYSCTL_PERIPH_GPIOC, //
				.int_gpio_base = GPIO_PORTC_BASE, //
				.int_gpio_pin = GPIO_PIN_6, //
				.int_gpio_int_pin = GPIO_INT_PIN_6 },

		//booster 2
		{ .ssi_base = SSI3_BASE, //
				.ssi_periph = SYSCTL_PERIPH_SSI3, //
				.ssi_gpio_periph = SYSCTL_PERIPH_GPIOQ, //
				.ssi_gpio_base = GPIO_PORTQ_BASE, //

				.ssi_gpio_afsel_mosi = GPIO_PQ3_SSI3XDAT1, //
				.ssi_gpio_mosi_pin = GPIO_PIN_3, //

				.ssi_gpio_afsel_miso = GPIO_PQ2_SSI3XDAT0, //
				.ssi_gpio_miso_pin = GPIO_PIN_2, //

				.ssi_gpio_afsel_clk = GPIO_PQ0_SSI3CLK, //
				.ssi_gpio_clk_pin = GPIO_PIN_0, //

				.cs_gpio_periph = SYSCTL_PERIPH_GPIOP, //
				.cs_gpio_base = GPIO_PORTP_BASE, //
				.cs_gpio_pin = GPIO_PIN_5, //
				.cs_shift = 5, //

				.ce_gpio_periph = SYSCTL_PERIPH_GPIOP, //
				.ce_gpio_base = GPIO_PORTP_BASE, //
				.ce_gpio_pin = GPIO_PIN_4, //
				.ce_shift = 4,

				.int_gpio_periph = SYSCTL_PERIPH_GPIOD, //
				.int_gpio_base = GPIO_PORTD_BASE, //
				.int_gpio_pin = GPIO_PIN_4, //
				.int_gpio_int_pin = GPIO_INT_PIN_4}};

/* ###################################
 * Global Variables
 * ################################### */

volatile uint32_t _nrfc_sysclock;
struct _nrfc_mappings *_nrfc_receiver = 0;
struct _nrfc_mappings *_nrfc_transmitter = 0;
void (*_nrfc_received_byte_handler)(uint8_t);

/* ###################################
 * Internal Implementation
 * ################################### */

/**
 * Set Channel Select pin value.
 * For nRF C, CS defaults to 1 and is pulled down during commands.
 */
void _nrfc_cs(struct _nrfc_mappings *mappings, uint8_t val) {
	uint8_t pins = val << mappings->cs_shift;
	GPIOPinWrite(mappings->cs_gpio_base, mappings->cs_gpio_pin, pins);
}

/**
 * Set Channel Enabled pin value.
 * For nRF C, setting CE triggers active modes (receive/transmit mode).
 */
void _nrfc_ce(struct _nrfc_mappings *mapping, uint8_t val) {
	uint8_t pins = val << mapping->ce_shift;
	GPIOPinWrite(mapping->ce_gpio_base, mapping->ce_gpio_pin, pins);
}

/**
 * Low-level SPI read/write operation.
 * In the SPI protocol, there must be a write for every read and vice versa.
 */
uint32_t _nrfc_spi_rw(uint32_t ssi_base, uint32_t byte) {
	uint32_t val = 0;
	SSIDataPut(ssi_base, byte);
	SSIDataGet(ssi_base, &val);
	return val;
}

/**
 * Execute an nRF C command: Write the command byte, read the result byte.
 */
uint32_t _nrfc_exec_command(struct _nrfc_mappings *mappings, uint32_t cmd, uint8_t writeval) {
	_nrfc_cs(mappings, 0);
	uint32_t status = _nrfc_spi_rw(mappings->ssi_base, cmd);
	uint32_t val = _nrfc_spi_rw(mappings->ssi_base, writeval);
	_nrfc_cs(mappings, 1);
	return val;
}

/**
 * Write a byte buffer to nRF C
 */
void _nrfc_spi_write_buf(struct _nrfc_mappings *mappings, uint32_t reg,
		uint8_t *pBuf, uint32_t bytes) {

	_nrfc_cs(mappings, 0);
	_nrfc_spi_rw(mappings->ssi_base, reg);

	uint32_t i;
	for (i = 0; i < bytes; i++) {
		_nrfc_spi_rw(mappings->ssi_base, *pBuf++);
	}

	_nrfc_cs(mappings, 1);
}

/**
 * Internal validation function to trap execution when an invalid
 * system or program state is detected.
 */
void _nrfc_assert_true(uint8_t condition) {
	/*
	 * TODO: Flashing a LED would help noticing errors without an active
	 * debug session
	 */
	while (!condition);
}

/**
 * Test SPI communication.
 * The nRF C click shifts out the STATUS register while reading a command.
 * By comparing this shifted status value to the result of an actual
 * register read on the STATUS register, we can verify that basic SPI comm is working.
 */
void _nrfc_test_communication(struct _nrfc_mappings *mappings) {
	_nrfc_cs(mappings, 0);
	SSIDataPut(mappings->ssi_base, READ_REG + STATUS);

	uint32_t status1 = 0;
	SSIDataGet(mappings->ssi_base, &status1);

	SSIDataPut(mappings->ssi_base, 0);
	uint32_t status2 = 0;
	SSIDataGet(mappings->ssi_base, &status2);

	_nrfc_cs(mappings, 1);

	_nrfc_assert_true(status1);
	_nrfc_assert_true(status1 == status2);
}

/**
 * Reads an address buffer from a register.
 */
void _nrfc_read_address(struct _nrfc_mappings *mappings, uint32_t reg, uint8_t addr[ADR_LENGTH]) {
	_nrfc_cs(mappings, 0);
	uint32_t status = _nrfc_spi_rw(mappings->ssi_base, READ_REG + reg);
	int i;
	for (i = 0; i < ADR_LENGTH; i++) {
		addr[i] = _nrfc_spi_rw(mappings->ssi_base, 0);
	}
	_nrfc_cs(mappings, 1);
}

/**
 * Configure wireless channel and other common transmission properties.
 */
void _nrfc_rf_setup(struct _nrfc_mappings* mappings) {
	// Enable Auto.Ack:Pipes 0-5
	_nrfc_exec_command(mappings, WRITE_REG + EN_AA, 0x3f);
	// Enable Pipes 0-5
	_nrfc_exec_command(mappings, WRITE_REG + EN_RXADDR, 0x3f);

	// Select RF channel 40
	_nrfc_exec_command(mappings, WRITE_REG + RF_CH, 40);
	// TX_PWR:0dBm, Datarate:2Mbps, LNA:HCURR
	_nrfc_exec_command(mappings, WRITE_REG + RF_SETUP, 0x0f);
}

/**
 * Initiate TX mode for transmitting data.
 */
void _nrfc_tx_mode(struct _nrfc_mappings *mappings) {
	// maximum retransmission delay and count
	_nrfc_exec_command(mappings, WRITE_REG + SETUP_RETR, 0xff);
	// Set PWR_UP bit, enable CRC(2 bytes) & Prim:TX. MAX_RT & TX_DS enabled
	_nrfc_exec_command(mappings, WRITE_REG + CONFIG, 0x0e);

	// Writes TX_Address to nRF24L01
	_nrfc_spi_write_buf(mappings, WRITE_REG + TX_ADDR, _NRFC_PIPE_ADDRESS, sizeof(_NRFC_PIPE_ADDRESS));
	// RX_Addr0 same as TX_Adr for Auto.Ack
	_nrfc_spi_write_buf(mappings, WRITE_REG + RX_ADDR_P0, _NRFC_PIPE_ADDRESS, sizeof(_NRFC_PIPE_ADDRESS));

	SysCtlDelay(_nrfc_sysclock / 3);
}

/**
 * Initiate RX mode for receiving data.
 */
void _nrfc_rx_mode(struct _nrfc_mappings *mappings) {
	// Set PWR_UP bit, enable CRC(2 bytes) & Prim:RX. RX_DR enabled
	_nrfc_exec_command(mappings, WRITE_REG + CONFIG, 0x0f);

	// Use the same address on the RX device as the TX device
	_nrfc_spi_write_buf(mappings, WRITE_REG + RX_ADDR_P0, _NRFC_PIPE_ADDRESS, sizeof(_NRFC_PIPE_ADDRESS));

	// Select same RX payload width as TX Payload width
	_nrfc_exec_command(mappings, WRITE_REG + RX_PW_P0, _NRFC_PAYLOAD_WIDTH);

	_nrfc_ce(mappings, 1);

	//rx mode takes some time, but actually way less than this delay
	SysCtlDelay(_nrfc_sysclock / 3);
}

void _nrfc_clear_interrupts(struct _nrfc_mappings *mappings) {
	_nrfc_exec_command(mappings, WRITE_REG + STATUS, MASK_IRQ_FLAGS);
}

void _nrfc_init_ssi(struct _nrfc_mappings *mappings, void (*isr)(void)) {
	/*
	 * This first part of initialization is based on the SPI Kochrezept
	 * provided for the EBS course.
	 */

	/* Aktivieren der GPIO Ports an denen das SPI Interface angeschlossen sein soll */
	SysCtlPeripheralEnable(mappings->ssi_gpio_periph);
	SysCtlPeripheralEnable(mappings->cs_gpio_periph);
	SysCtlPeripheralEnable(mappings->ce_gpio_periph);

	/* Aktiviren des gewünschten SPI Peripherals */
	SysCtlPeripheralEnable(mappings->ssi_periph);

	/* Setzen der entsprechenden alternativen Pinfunktionen */
	GPIOPinConfigure(mappings->ssi_gpio_afsel_mosi);
	GPIOPinConfigure(mappings->ssi_gpio_afsel_miso);
	GPIOPinConfigure(mappings->ssi_gpio_afsel_clk);

	GPIOPinTypeGPIOOutput(mappings->cs_gpio_base, mappings->cs_gpio_pin);
	GPIOPinTypeGPIOOutput(mappings->ce_gpio_base, mappings->ce_gpio_pin);

	/* Setzen Treiberstärke und Richtung für jene SPI Pins */
	GPIOPinTypeSSI(mappings->ssi_gpio_base, mappings->ssi_gpio_mosi_pin);
	GPIOPinTypeSSI(mappings->ssi_gpio_base, mappings->ssi_gpio_miso_pin);
	GPIOPinTypeSSI(mappings->ssi_gpio_base, mappings->ssi_gpio_clk_pin);

	/* Festlegen der Taktquelle für das SPI Modul */
	SSIClockSourceSet(mappings->ssi_base, SSI_CLOCK_SYSTEM);

	/* Konfigurieren sie den Taktteiler und die Betriebsmodi des SPI Moduls */
	SSIConfigSetExpClk(mappings->ssi_base, //
			_nrfc_sysclock, //
			SSI_FRF_MOTO_MODE_0, // polarity 0 phase 0
			SSI_MODE_MASTER, //
			2000000, // bit rate
			8); // data width

	/* Aktivieren Sie das SPI Peripheriegerät SSIEnable(); */
	SSIEnable(mappings->ssi_base);

	while (SSIBusy(mappings->ssi_base))
		;

	/*
	 * Second part of initialization: nRF C specific init
	 */

	// Set CE pin low to enable standby mode
	_nrfc_ce(mappings, 0);
	_nrfc_cs(mappings, 1);


	/*
	 * skip leftovers in FIFO
	 */
	uint32_t leftovers = 0;
	while (SSIDataGetNonBlocking(mappings->ssi_base, &leftovers));

	_nrfc_test_communication(mappings);

	if (isr != 0) {
		SysCtlPeripheralEnable(mappings->int_gpio_periph);
		GPIOPinTypeGPIOInput(mappings->int_gpio_base, mappings->int_gpio_pin);
		GPIOPadConfigSet(mappings->int_gpio_base, mappings->int_gpio_pin, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);

		GPIOIntRegister(mappings->int_gpio_base, isr);
		GPIOIntTypeSet(mappings->int_gpio_base, mappings->int_gpio_pin, GPIO_FALLING_EDGE);
		GPIOIntEnable(mappings->int_gpio_base, mappings->int_gpio_int_pin);
	}

	_nrfc_exec_command(mappings, FLUSH_TX, 0);
	_nrfc_exec_command(mappings, FLUSH_RX, 0);

	// Clear interrupts
	_nrfc_clear_interrupts(mappings);

	_nrfc_rf_setup(mappings);
}

void _nrfc_received_isr() {
	GPIOIntClear(_nrfc_receiver->int_gpio_base, _nrfc_receiver->int_gpio_int_pin);

	/*
	 * This ISR is based on the nRFc data sheet / manual (reflected in the comments)
	 */

	//	The RX_DR IRQ is asserted by a new packet arrival event.

	// 1) read payload through SPI
	uint32_t received_byte = _nrfc_exec_command(_nrfc_receiver, RD_RX_PLOAD, 0);

	// 2) clear RX_DR IRQ
	_nrfc_clear_interrupts(_nrfc_receiver);

	_nrfc_received_byte_handler(received_byte);

	// 3) read FIFO_STATUS to check if there are more payloads available in RX FIFO
	// 4) if there are more data in RX FIFO, repeat from step 1)

	//TODO: This is not implemented in our solution.
}

void _nrfc_transmitted_isr() {
	GPIOIntClear(_nrfc_transmitter->int_gpio_base, _nrfc_transmitter->int_gpio_int_pin);
	_nrfc_clear_interrupts(_nrfc_transmitter);
}

/**
 * See _nrfc_pulse_ce
 */
void _nrfc_pulse_timer_isr() {
	TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	GPIOPinWrite(_nrfc_transmitter->ce_gpio_base, _nrfc_transmitter->ce_gpio_pin, 0);
}

/**
 * See _nrfc_pulse_ce
 */
void _nrfc_init_pulse_timer() {
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

	TimerConfigure(TIMER0_BASE, TIMER_CFG_ONE_SHOT);

	TimerIntRegister(TIMER0_BASE, TIMER_A, _nrfc_pulse_timer_isr);
	TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

	TimerEnable(TIMER0_BASE, TIMER_A);
}

/**
 * Pulses the CE pin to trigger transmission mode.
 * This is a powersaving feature of the nRF C. 'Pulsing' means that the CE bit
 * is pulled to 1 for a very short time. It needs to be at 1 for at least 10 microseconds.
 */
void _nrfc_pulse_ce(struct _nrfc_mappings *mappings) {
	uint8_t pins = 1 << mappings->ce_shift;
	GPIOPinWrite(mappings->ce_gpio_base, mappings->ce_gpio_pin, pins);

	TimerLoadSet(TIMER0_BASE, TIMER_A, 110);
}

uint32_t _nrfc_read_register(struct _nrfc_mappings *mappings, uint32_t reg) {
	return _nrfc_exec_command(mappings, READ_REG + reg, 0);
}

/**
 * Internal test function that reads many registers commonly used for debugging.
 */
void _nrfc_diagnostics(struct _nrfc_mappings *mappings) {
	/*
	 * config diagnostics
	 */
	uint32_t config = _nrfc_read_register(mappings, CONFIG);
	uint32_t aw = _nrfc_read_register(mappings, SETUP_AW);
	uint32_t txaddr = _nrfc_read_register(mappings, TX_ADDR);
	uint32_t rfsetup = _nrfc_read_register(mappings, RF_SETUP);
	uint32_t cd = _nrfc_read_register(mappings, CD); // carrier detect

	uint8_t rxaddr0[ADR_LENGTH];
	_nrfc_read_address(mappings, RX_ADDR_P0, rxaddr0);

	/*
	 * transmission status diagnostics
	 */
	uint32_t status = _nrfc_read_register(mappings, STATUS);
	uint32_t observe = _nrfc_read_register(mappings, OBSERVE_TX);
	uint32_t fifo = _nrfc_read_register(mappings, FIFO_STATUS);
	uint32_t enaa = _nrfc_read_register(mappings, EN_AA); //auto ack
	uint32_t rxaddr = _nrfc_read_register(mappings, EN_RXADDR);
}

void _nrfc_validate_booster_slot(uint8_t booster_slot) {
	//Only need to check upper boundary because unsigned int can't be < 0
	_nrfc_assert_true(booster_slot < _NRFC_BOOSTER_COUNT);
}

/* ###################################
 * Public API
 * ################################### */

void nrfc_init_transmitter(uint32_t sysclock, uint8_t booster_slot) {
	_nrfc_sysclock = sysclock;
	_nrfc_validate_booster_slot(booster_slot);
	_nrfc_transmitter = &_nrfc_configs[booster_slot];

	_nrfc_init_pulse_timer();
	_nrfc_init_ssi(_nrfc_transmitter, _nrfc_transmitted_isr);

	_nrfc_tx_mode(_nrfc_transmitter);
}

void nrfc_init_receiver(uint32_t sysclock, uint8_t booster_slot, void (*pfnReceivedByteHandler)(uint8_t)) {
	_nrfc_sysclock = sysclock;
	_nrfc_validate_booster_slot(booster_slot);
	_nrfc_receiver = &_nrfc_configs[booster_slot];
	_nrfc_received_byte_handler = pfnReceivedByteHandler;

	_nrfc_init_ssi(_nrfc_receiver, _nrfc_received_isr);

	_nrfc_rx_mode(_nrfc_receiver);
}

void nrfc_send_byte(uint8_t byte) {
	_nrfc_assert_true(_nrfc_transmitter != 0);

	uint8_t msg[_NRFC_PAYLOAD_WIDTH];
	uint8_t i;
	for (i = 0; i < _NRFC_PAYLOAD_WIDTH; i++) {
		msg[i] = 0;
	}

	msg[0] = byte;
	_nrfc_spi_write_buf(_nrfc_transmitter, WR_TX_PLOAD, msg, _NRFC_PAYLOAD_WIDTH);

	_nrfc_pulse_ce(_nrfc_transmitter);
}
