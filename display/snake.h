/*
 * snake.h
 *
 *  Created on: 12.04.2016
 *      Author: fitzi
 */

#ifndef SNAKE_H_
#define SNAKE_H_

// status flags ( in a 16 bit register )
#define snake_state_active 0
#define snake_state_input_waiting 1
#define snake_state_input_click 2
#define snake_state_input_direction 3
#define snake_state_dead 5
#define snake_state_direction 7
#define snake_food_pos_x 9
#define snake_food_pos_y 12
#define snake_state_initialized 15

// snake node position
#define snake_pos_x 3
#define snake_pos_y 0

// gamefield size
#define snake_field_w 8
#define snake_field_h 8

// directions
#define snake_dir_top 0
#define snake_dir_right 1
#define snake_dir_bottom 2
#define snake_dir_left 3

// animation speeds
#define snake_frames_menu 10000000
#define snake_frames_xs 9000000
#define snake_frames_s 7000000
#define snake_frames_m 5000000
#define snake_frames_l 3000000
#define snake_frames_xl 1500000
#define snake_frames_animation 1000000

// single node of snake
struct snake {
	uint8_t position; // 0 bit set, 1 unused, 2 - 4 bit position x, 5 - 7 bit position y
	struct snake *next;
	struct snake *prev;
};

// Initializes the game. Must not be called more than once
void snake_init();

// Resets board, points and snake to initial state
void snake_reset();

// Starts a new game (calls reset)
void snake_start();

// Stops the game
void snake_stop();

// is called when snake hits a wall or itself
// sets game into dead mode (death-animation is played)
// calls snake_stop after animation is finished
void snake_game_dead();

// Is called periodically by the timer. Updates the game.
extern void snake_isr(void);

// Change the intervall, snake_isr is beeing called
void snake_set_speed( uint32_t speed );

// Renders the welcome screen
void snake_update_welcome();

// Updates the game. Moves snake, renders new frame or sets snake status to dead
void snake_update_game();

// Checks if game field is empty
bool snake_is_empty_node( uint8_t x, uint8_t y );

// Generates a new food element
void snake_generate_food();

// updates player inputs
void snake_input_click();
void snake_input( uint8_t direction );


// HELPERS
// returns x or y coordinate of snakes head
uint8_t snake_get_curr_head_position( uint8_t axis );

// adds snakes next position on x/y coordiantes
void snake_add_next_position( uint8_t x, uint8_t y );

// removes last node of snake
void snake_shorten_tail();

// gets single status bit of state register
bool snake_state_get( uint8_t position );

// sets a single status bit
void snake_state_set( uint8_t position, bool value );

// returns current direction snake is heading to
uint8_t snake_state_get_current_direction();

// retusn input direction, only valid if input waiting flag is set
uint8_t snake_state_get_input_direction();

// updates the current direction
void snake_state_set_current_direction( uint8_t dir );

// updates the board variable, does not alter led module
void snake_render_game();

// outpus board variable to led module
void snake_render_board();

// Pseudorandom number generator
void snake_init_randomizer(uint8_t s1, uint8_t s2, uint8_t s3);
uint8_t snake_get_random();


#endif /* SNAKE_H_ */
