#include <stdint.h>
#include <stdbool.h>
#include "inc/tm4c1294ncpdt.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/systick.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "driverlib/timer.h"
#include "driverlib/ssi.h"

#include "ledm.h"
#include "snake.h"

#include "nrfc.h"
#include "score.h"

volatile uint8_t led_intensity;

void controller_input(uint8_t byte) {

	// byte:
	// 0 top
	// 1 rechts
	// 2 unten
	// 3 links
	// 4 click
	// 5 dim screen
	// 6 lighten screen

	if( byte == 6 ){

		// increase brightness
		if( led_intensity < ledm_intensity_max ){
			led_intensity++;
			ledm_set_intensity(led_intensity);
			ledm_wait();
		}

	}else if( byte == 5 ){

		// decrease brightness
		if( led_intensity > ledm_intensity_min ){
			led_intensity--;
			ledm_set_intensity(led_intensity);
			ledm_wait();
		}

	}else if( byte == 4 ){
		snake_input_click();
	}else{
		snake_input(byte);
	}

}

void init_hardware(){

	uint32_t sysclock;

	sysclock = SysCtlClockFreqSet( SYSCTL_XTAL_25MHZ | SYSCTL_USE_PLL | SYSCTL_CFG_VCO_480, 10000000);

	// init led module
	ledm_init(sysclock);

	// init receiver
	nrfc_init_receiver(sysclock, NRFC_BOOSTER_1, controller_input);

}

void main(void){

	init_hardware();

	// set no decode mode
	ledm_set(ledm_addr_decode, 0);
	ledm_wait();

	// disable test mode
	ledm_test_mode(false);
	ledm_wait();

	// update scan limit
	ledm_scan_limit(ledm_scanlimit_all);
	ledm_wait();

	// update brightness
	led_intensity = ledm_intensity_max;
	ledm_set_intensity(led_intensity);
	ledm_wait();

	// disable shutdown mode
	ledm_shutdown_mode(false);
	ledm_wait();

	// init score counter
	score_init();

	// init snake
	snake_init();

	while(1);

}
