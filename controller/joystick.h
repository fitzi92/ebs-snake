
#ifndef JOYSTICK_H_
#define JOYSTICK_H_

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include "i2c_wrapper.h"

#define I2C_SLAVE_ADDR 0x40
#define C1_REG 0x0F					   // 	 Controlregister 1
#define X_REG 0x10					   //	 Result X
#define Y_REG 0x11                      //    Result Y
#define _XP 0x12                        //    Positive X direction
#define _XN 0x13                        //    Negative X direction
#define _YP 0x14                        //    Positive Y direction
#define _YN 0x15                        //    Negative Y direction
#define _AGC 0x2A                       //    AGC
#define _M_CTRL 0x2B                    //    Control register for the middle Hall element C5
#define _J_CTRL 0x2C                    //    Control register for the sector dependent attenuation of the outer Hall elements
#define _T_CTRL 0x2D                    //    Scale input to fit to the 8 bit result register
#define _Control2 0x2E                  //    Control register 2

#define BOOSTERPACK_1 0
#define BOOSTERPACK_2 1
#define JOYSTICK_BOOSTER_COUNT 2

extern uint8_t buf[2];
extern _Bool xnegative;
extern _Bool ynegative;
extern int8_t x_reg;
extern int8_t y_reg;
extern uint8_t y_abs;
extern uint8_t x_abs;
extern int8_t direction;
extern volatile uint32_t sysclock;

/*
 * initialising joystick hardware
 *
 * Parameters:
 * 	 boosterpack: The boosterpack the hardware ist connected to (either BOOSTERPACK_1 or BOOSTERPACK_2)
 */
void init_joystick(uint8_t boosterpack);

/*
 * There has to be an initial read of the Y register to activate automatic read on joystick movement
 */
void initial_read();

/*
 * sets the absolute value of X and the xnegative flag
 */
void getX();

/*
 * sets the absolute value of Y and the ynegative flag
 */
void getY();

/*
 * calculates the direction in wich the player wants to move the snake
 * 0 - up
 * 1 - right
 * 2 - down
 * 3 - left
 * -1 - undiceded
 */
void getDirection();

#endif /* JOYSTICK_H_ */
