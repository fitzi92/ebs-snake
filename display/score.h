/*
 * # score.h
 *
 * This is the API for a peripheral used to display Snake scores.
 */

#ifndef SCORE_H_
#define SCORE_H_

#define MAX_SCORE 64

/**
 * Initialize the component(s) used by the score peripheral.
 */
void score_init();

/**
 * Writes the score to the peripheral.
 *
 * Parameters:
 * 	 score: The value to write. Must not exceed MAX_SCORE.
 *
 */
void score_write(uint8_t score);

#endif /* SCORE_H_ */
