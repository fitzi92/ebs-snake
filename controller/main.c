#include "joystick.h"
#include "inc/tm4c1294ncpdt.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_i2c.h"
#include "driverlib/i2c.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/systick.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "nrfc.h"

/*
 * initializes the interrupt for the movement and click functionality on the joystick
 */
void init_interrupts(void);

/*
 * initializes the hardware and interrupts for on board push buttons (adjusting brightness)
 */
void init_hardware(void);

/*
 * sets the newValue flag on true
 */
void joystickMovedInterrupt(void);

/*
 * sets the clicked flag on true
 */
void joystickClickedInterrupt(void);

/*
 * not in use
 */
uint8_t waitForInterruptReset(void);

/*
 * not in use
 */
void resetInterrupt(void);

/*
 * switch interrupt functions regulated the brightness of the "screen"
 */
void switch2PressInterrupt(void);
void switch1PressInterrupt(void);

volatile _Bool newValue = false;
volatile _Bool clicked = false;
volatile _Bool switch1Pressed = false;
volatile _Bool switch2Pressed = false;
uint8_t buf[2];
_Bool xnegative = false;
_Bool ynegative = false;
int8_t x_reg;
int8_t y_reg;
uint8_t y_abs;
uint8_t x_abs;
int8_t direction = -1;
volatile uint32_t sysclock;

struct hardware_mapping {
	uint32_t move_peripheral;
	uint32_t move_port;
	uint32_t move_pin;
	uint32_t move_int_pin;

	uint32_t click_peripheral;
	uint32_t click_port;
	uint32_t click_pin;
	uint32_t click_int_pin;

	uint8_t i2c_modul;
};

struct hardware_mapping hardware_config[JOYSTICK_BOOSTER_COUNT] = {
		//booster 1
		{
			.move_peripheral = SYSCTL_PERIPH_GPIOC,
			.move_port = GPIO_PORTC_BASE,
			.move_pin = GPIO_PIN_6,
			.move_int_pin = GPIO_INT_PIN_6,

			.click_peripheral = SYSCTL_PERIPH_GPIOH,
			.click_port = GPIO_PORTH_BASE,
			.click_pin = GPIO_PIN_2,
			.click_int_pin = GPIO_INT_PIN_2,

			.i2c_modul = I2CM_7
		},
		//booster 2
		{
			.move_peripheral = SYSCTL_PERIPH_GPIOD,
			.move_port = GPIO_PORTD_BASE,
			.move_pin = GPIO_PIN_4,
			.move_int_pin = GPIO_INT_PIN_4,

			.click_peripheral = SYSCTL_PERIPH_GPIOP,
			.click_port = GPIO_PORTP_BASE,
			.click_pin = GPIO_PIN_5,
			.click_int_pin = GPIO_INT_PIN_5,

			.i2c_modul = I2CM_8
		}
};

volatile struct hardware_mapping *joystick_hardware = 0;

void main(void) {
	int8_t oldDirection = direction;

	/*change the CPU Hz*/
	sysclock = SysCtlClockFreqSet(SYSCTL_XTAL_25MHZ | SYSCTL_USE_PLL | SYSCTL_CFG_VCO_480, 10*1000*1000);

	joystick_hardware = &hardware_config[BOOSTERPACK_1];

	i2c_init(joystick_hardware->i2c_modul, sysclock, HIGH_400_kHz);
	init_joystick(BOOSTERPACK_1);
	init_hardware();
	init_interrupts();
    IntMasterEnable();

	initial_read();

	//nrfc_init_transmitter(sysclock, NRFC_BOOSTER_2);

    while(1){
    		if(newValue){
    			getX();
    			getY();
    			getDirection();
    			if(direction != -1 && oldDirection != direction){
    				//nrfc_send_byte(direction);
				oldDirection = direction;
    			}
			newValue = false;
			//resetInterrupt();
			GPIOIntEnable(joystick_hardware->move_port, joystick_hardware->move_pin);
    		}
    		if(clicked){
    			//nrfc_send_byte(4);
    			clicked = false;
    			oldDirection = -1;
    			GPIOIntEnable(joystick_hardware->click_port, joystick_hardware->click_pin);
    		}
    }
}

void init_interrupts(void) {
	SysCtlPeripheralEnable(joystick_hardware->move_peripheral);
	GPIOPinTypeGPIOInput(joystick_hardware->move_port, joystick_hardware->move_pin);
	GPIOIntTypeSet(joystick_hardware->move_port, joystick_hardware->move_pin, GPIO_LOW_LEVEL);
	GPIOIntEnable(joystick_hardware->move_port, joystick_hardware->move_int_pin);
	GPIOIntRegister(joystick_hardware->move_port, joystickMovedInterrupt);
	SysCtlPeripheralEnable(joystick_hardware->click_peripheral);
	GPIOPinTypeGPIOInput(joystick_hardware->click_port, joystick_hardware->click_pin);
	GPIOIntTypeSet(joystick_hardware->click_port, joystick_hardware->click_pin, GPIO_FALLING_EDGE);
	GPIOIntEnable(joystick_hardware->click_port, joystick_hardware->click_int_pin);
	GPIOIntRegister(joystick_hardware->click_port, joystickClickedInterrupt);
}

void init_hardware(void){
	uint32_t ui32Strength, ui32PinType;
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOJ);
	GPIOPinTypeGPIOInput(GPIO_PORTJ_BASE,GPIO_PIN_0);
	GPIOPadConfigGet(GPIO_PORTJ_BASE, GPIO_PIN_0, &ui32Strength, &ui32PinType);
	GPIOPadConfigGet(GPIO_PORTJ_BASE, GPIO_PIN_1, &ui32Strength, &ui32PinType);
	GPIOPadConfigSet(GPIO_PORTJ_BASE, GPIO_PIN_0, ui32Strength, GPIO_PIN_TYPE_STD_WPU);
	GPIOPadConfigSet(GPIO_PORTJ_BASE, GPIO_PIN_1, ui32Strength, GPIO_PIN_TYPE_STD_WPU);
	GPIOIntTypeSet(GPIO_PORTJ_BASE, GPIO_PIN_0, GPIO_FALLING_EDGE);
	GPIOIntEnable(GPIO_PORTJ_BASE, GPIO_INT_PIN_0);
	GPIOIntRegister(GPIO_PORTJ_BASE, switch2PressInterrupt);
	GPIOIntTypeSet(GPIO_PORTJ_BASE, GPIO_PIN_1, GPIO_FALLING_EDGE);
	GPIOIntEnable(GPIO_PORTJ_BASE, GPIO_INT_PIN_1);
}

void joystickMovedInterrupt(void){
	GPIOIntClear(joystick_hardware->move_port, joystick_hardware->move_pin);
	GPIOIntDisable(joystick_hardware->move_port, joystick_hardware->move_pin);
	newValue = true;
}

void joystickClickedInterrupt(void){
	GPIOIntClear(joystick_hardware->click_port, joystick_hardware->click_pin);
	GPIOIntDisable(joystick_hardware->click_port, joystick_hardware->click_pin);
	clicked = true;
}

uint8_t waitForInterruptReset(void){
	return GPIOPinRead(joystick_hardware->move_port, joystick_hardware->move_pin);
}

void switch1PressInterrupt(){
	GPIOIntDisable(GPIO_PORTJ_BASE, GPIO_INT_PIN_0);
	if(!switch1Pressed){
		nrfc_send_byte(5);
		switch1Pressed = true;
	}
	switch1Pressed = false;
	GPIOIntEnable(GPIO_PORTJ_BASE, GPIO_INT_PIN_0);
}

void switch2PressInterrupt(void){
	uint32_t xy = GPIOIntStatus(GPIO_PORTJ_BASE, true);
	if(xy == 1){
		GPIOIntClear(GPIO_PORTJ_BASE, GPIO_PIN_0);
		switch1PressInterrupt();
	} else {
		GPIOIntClear(GPIO_PORTJ_BASE, GPIO_PIN_1);
		GPIOIntDisable(GPIO_PORTJ_BASE, GPIO_INT_PIN_1);
		if(!switch2Pressed){
			nrfc_send_byte(6);
			switch2Pressed = true;
		}
		switch2Pressed = false;
		GPIOIntEnable(GPIO_PORTJ_BASE, GPIO_INT_PIN_1);
	}
}

void resetInterrupt(){
	uint8_t counter = 0;
	/*Loops 15 times an checks if Pin is high, after 15 times tries to read Y-Coordinate an reset ACK-bit*/
	while(1){
		if(waitForInterruptReset() == 0){
			counter++;
			if(counter > 15){
				counter = 0;
				getY();
			}
		} else{
			break;
		}
	}
}
