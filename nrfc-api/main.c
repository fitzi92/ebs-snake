#include <stdint.h>
#include <stdbool.h>
#include "inc/tm4c1294ncpdt.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/systick.h"

#include "nrfc.h"

/* ###################################
 * Global variables
 * ################################### */

volatile uint32_t sysclock;

void init_led() {
	uint32_t ui32Strength;
	uint32_t ui32PinType;

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
	GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_0);

	GPIOPadConfigGet(GPIO_PORTN_BASE, GPIO_PIN_0, &ui32Strength, &ui32PinType);
	GPIOPadConfigSet(GPIO_PORTN_BASE, GPIO_PIN_0, ui32Strength,
			GPIO_PIN_TYPE_STD);
}

void init_sysclock() {
	sysclock = SysCtlClockFreqSet(SYSCTL_XTAL_25MHZ | SYSCTL_USE_PLL | SYSCTL_CFG_VCO_480, 10 * 1000 * 1000);
}

void switch_on_led(uint8_t byte) {
	GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 1);
}

void main() {
	init_led();
	init_sysclock();
	IntMasterEnable();

	nrfc_init_transmitter(sysclock, NRFC_BOOSTER_2);
	nrfc_init_receiver(sysclock, NRFC_BOOSTER_1, switch_on_led);

	for (;;) {
		GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 0);
		SysCtlDelay(sysclock / 6);
		nrfc_send_byte(1);
		SysCtlDelay(sysclock / 6);
	}
}
